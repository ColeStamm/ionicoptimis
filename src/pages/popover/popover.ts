import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-content',
  templateUrl: 'popover.html'
})
export class PopoverPage {

  constructor(public navCtrl: NavController) { }

}
