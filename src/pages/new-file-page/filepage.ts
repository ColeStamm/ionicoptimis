import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';

/**
 * The Welcome Page is a splash page that quickly describes the app,
 * and then directs the user to create an account or log in.
 * If you'd like to immediately put the user onto a login/signup page,
 * we recommend not using the Welcome page.
*/
@IonicPage()
@Component({
  selector: 'page-welcome',
  templateUrl: 'filepage.html'
})
export class FilePage {

  constructor(public navCtrl: NavController) { }

  mainMenu() {
    this.navCtrl.setRoot('NewRecord');
  }

  cancel() {
    this.navCtrl.setRoot('WelcomePage');
  }
}
