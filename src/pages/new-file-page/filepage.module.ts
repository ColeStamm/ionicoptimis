import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';


import { FilePage } from './filepage';

@NgModule({
  declarations: [
    FilePage,
  ],
  imports: [
    IonicPageModule.forChild(FilePage),
    TranslateModule.forChild()
  ],
  exports: [
    FilePage
  ]
})
export class FilePageModule { }
