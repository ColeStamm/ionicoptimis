import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-new-record',
  templateUrl: 'new-record.html'
})
export class NewRecord {
  indexVar: any;
  gradingCats: any;


  constructor(public navCtrl: NavController) {
    this.indexVar = 1;
    this.gradingCats = [
      {
        title: "Aircraft Utilization",
        img: "assets/img/menu-icons/aircraftutilization_btn_u184.png",
        slug: "aircraft-ultilization",
        gradingObjects: [
          {title: "AFCS Ops", slug: "afsc-ops"},
          {title: "Checklist", slug: "checklist"},
          {title: "Ground Ops", slug: "ground-ops"},
          {title: "Time Control", slug: "time-control"},
          {title: "TOLD/Dep Plan/Msn Comp", slug: "told-dep-plan-msn-comp"},
          {title: "WX Radar Ops", slug: "wx-radar-ops"} ]
      },
      {
        title: "Air Refueling",
        img: "assets/img/menu-icons/airrefueling_btn_u160.png",
        slug: "air-refueling",
        gradingObjects: [
          {title: "AAR Day AP On", slug: "aar-day-ap-on"},
          {title: "AAR Day AP Off", slug: "aar-day-ap-off"},
          {title: "AAR Night AP On", slug: "aar-night-ap-on"},
          {title: "AAR Night AP Off", slug: "aar-night-ap-off"},
          {title: "AAR RZ", slug: "aar-rz"}]
      },
      {
        title: "Airmanship",
        img: "assets/img/menu-icons/airmanship__btn_u162.png",
        slug: "airmanship",
        gradingObjects: [
          {title: "CRM", slug: "crm"},
          {title: "Judgement", slug: "judegment"},
          {title: "Safety", slug: "safety"},
          {title: "Sit Awareness", slug: "sit-awareness"}]
      },
      {
        title: "Assault",
        img: "assets/img/menu-icons/assault_btn_u182.png",
        slug: "assault",
        gradingObjects: [
          {title: "ALZ Day/Night", slug: "alz-day-night"},
          {title: "ALZ NVG", slug: "alz-nvg"}]
      },
      {
        title: "Cargo",
        img: "assets/img/menu-icons/cargo_btn_u164.png",
        slug: "cargo",
        gradingObjects: [
          {title: "ERO/COP Taxi", slug: "ero-cop-taxi"},
          {title: "LM Checklist", slug: "lm checklist"},
          {title: "LM Msn Prep Plan", slug: "lm-msn-prep-plan"},
          {title: "LM Preflight", slug: "lm-preflight"},
          {title: "LM Systems GK", slug: "lm-systems-gk"},
          {title: "Load Plan/Insp", slug: "load-plan-insp"},
          {title: "On/Off Load", slug: "on-off-load"},
          {title: "PAX Handling", slug: "pax-handling"},
          {title: "W & B", slug: "w-b"}]
      },
      {
        title: "General Knowledge",
        img: "assets/img/menu-icons/generalknowledge_btn_u166.png",
        slug: "general-knowledge",
        gradingObjects: [
          {title: "GK Procedural", slug: "gk-procedral"},
          {title: "GK Systems", slug: "gk-systems"},
          {title: "GK Tactical", slug: "gk-tactical"}]
      },
      {
        title: "Instruction",
        img: "assets/img/menu-icons/instruction_btn_u168.png",
        slug: "instruction",
        gradingObjects: [
          {title: "Inst Ability", slug: "inst-ability"},
          {title: "Inst Tac Ability", slug: "inst-tac-ability"}]
      },
      {
        title: "Instruments",
        img: "assets/img/menu-icons/instruments_btn_u170.png",
        slug: "instruments",
        gradingObjects: [
          {title: "Holding", slug: "holding"},
          {title: "NonPrec IAP", slug: "nonprec-iap"},
          {title: "RNAV", slug: "rnav"},
          {title: "SID/STAR", slug: "sid-star"}]
      },
      {
        title: "Landings",
        img: "assets/img/menu-icons/landings_btn_u172.png",
        slug: "landings",
        gradingObjects: [
          {title: "3/4 Flap Land", slug: "34-flap-land"},
          {title: "VFR Pattern", slug: "vfr-pattern"},
          {title: "FF Heavy Land", slug: "ff-heavy-land"},
          {title: "GA/MAP", slug: "ga-map"},
          {title: "Full Flap Land", slug: "full-flap-land"}]
      },
      {
        title: "Low Levels",
        img: "assets/img/menu-icons/lowlevels_btn_u174.png",
        slug: "low-levels",
        gradingObjects: [
          {title: "LL Execution", slug: "ll-execution"},
          {title: "LL planning", slug: "ll-planning"}]
      },
      {
        title: "Pilot Communication",
        img: "assets/img/menu-icons/pilotcommunications_btn_u176.png",
        slug: "pilot-communication",
        gradingObjects: [
          {title: "Brief", slug: "brief"},
          {title: "C2", slug: "c2"},
          {title: "Comms/ATC Compliance", slug: "comms-atc-compliance"},
          {title: "Msn Mgmt", slug: "msn-mgmt"},
          {title: "Msn Mgmt Tac", slug: "msn-mgmt-tac"},
          {title: "NAT/OC", slug: "nat-oc"},
          {title: "VVM/PM Duties", slug: "vvm-pm-duties"}]
      },
      {
        title: "Planning",
        img: "assets/img/menu-icons/planning_btn_u178.png",
        slug: "planning",
        gradingObjects: [
          {title: "Msn Plan", slug: "msn-plan"},
          {title: "Msn Plan Tac", slug: "msn-plan-tac"}]
      },
      {
        title: "Tactics",
        img: "assets/img/menu-icons/tactics_btn_u180.png",
        slug: "tactics",
        gradingObjects: [
          {title: "Reactive TTPs", slug: "reactive-ttps"},
          {title: "The Arrival", slug: "the-arrival"},
          {title: "TAC Departure", slug: "tac-departure"},
          {title: "Threat Avoidance", slug: "threat-avoidance"}]
      }

    ];
  }

  goTo(btn){
    console.log("GO TO INIATED")
    console.dir(btn);
    this.navCtrl.push("ExampleTemplate", {
      data: {btn}
    });
  }

  exitToFilePage(btn){
    console.log("GO TO INIATED")
    console.dir(btn);
    this.navCtrl.setRoot("FilePage", {
    });
  }

}
