import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';

import { NewRecord } from './new-record';

@NgModule({
  declarations: [
    NewRecord,
  ],
  imports: [
    IonicPageModule.forChild(NewRecord),
    TranslateModule.forChild()
  ],
  exports: [
    NewRecord
  ]
})
export class NewRecordModule { }
