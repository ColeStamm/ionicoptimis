import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';


import { SelectFilePage } from './selectfilepage';

@NgModule({
  declarations: [
    SelectFilePage,
  ],
  imports: [
    IonicPageModule.forChild(SelectFilePage),
    TranslateModule.forChild()
  ],
  exports: [
    SelectFilePage
  ]
})
export class SelectFilePageModule { }
