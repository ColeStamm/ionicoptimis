import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';

import { ExampleTemplate } from './example-template';

@NgModule({
  declarations: [
    ExampleTemplate,
  ],
  imports: [
    IonicPageModule.forChild(ExampleTemplate),
    TranslateModule.forChild()
  ],
  exports: [
    ExampleTemplate
  ]
})
export class ExampleTemplateModule { }
//ll
