import { Component } from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';
import { ModalController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-example-template',
  templateUrl: 'example-template.html'
})
export class ExampleTemplate {

  gradingRows: any;
  pushedData: any;
  constructor(public navCtrl: NavController,public navParams: NavParams, public modalCtrl: ModalController) {

    this.pushedData = navParams.get('data');
    console.dir(this.pushedData);
    this.gradingRows = [{title: "title", description: "DESCSS"}, 'row2', 'row3', 'roew4']
  }

  goToNotes(){

    this.navCtrl.push("NotesPage", {

    });
  }

  submit(){
    this.navCtrl.setRoot("NewRecord", {

    });
  }
  cancel(){
    this.navCtrl.setRoot("NewRecord", {

    });

  }

  helpPopover(){
    // const popover = this.popoverCtrl.create("PopoverPage");
    // popover.present();
    const modal = this.modalCtrl.create("PopoverPage");
    modal.present();

  }
}
//aa
