import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';

import { GradingPage } from './grading';

@NgModule({
  declarations: [
    GradingPage,
  ],
  imports: [
    IonicPageModule.forChild(GradingPage),
    TranslateModule.forChild(),

  ],
  exports: [
    GradingPage
  ]
})
export class ContentPageModule { }
