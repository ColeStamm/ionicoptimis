import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';

import {AngularFirestore} from "angularfire2/firestore";

@IonicPage()
@Component({
  selector: 'page-content',
  templateUrl: 'gradingRoot.html'
})
export class GradingRoot {
  gradingRows: any;
  userProfileCollection: any;
  constructor(public navCtrl: NavController, public firestore: AngularFirestore) {
     this.gradingRows = [
       {
         title: "title",
         description: "DESCSS",
         grading: {
           gradingName: "TEST",
           row:2
         }
         },
      ]

    // this.userProfileCollection = firestore.collection('testCollection').add({name: 'John De', email: "test@test.com"});
    // this.userProfileCollection.push({
    //   name: 'Jorge Vergara',
    //   email: 'j@javebratt.com',
    //   // Other info you want to add here
    // });
  }

  goTo() {

    this.navCtrl.push("GradingPage", {
      data: {color: "black", name:"Dillon"}
    });
  }

}
