import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';

import { GradingRoot } from './gradingRoot';

@NgModule({
  declarations: [
    GradingRoot,
  ],
  imports: [
    IonicPageModule.forChild(GradingRoot),
    TranslateModule.forChild(),

  ],
  exports: [
    GradingRoot
  ]
})
export class ContentPageModule { }
